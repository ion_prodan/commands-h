var express = require('express');
var router = express.Router();
var async = require('async');
var mongoose = require('mongoose');
var cat = require('../models/category').Category;
var posts = require('../models/posts').Posts;

var allCategory = {
    title : 'Toate categoriile',
    read : false
};
router.get('*',function(req, res, next) {
    cat.find(function(err, all, affected){
        !err && next(err);
        allCategory.categoryList = affected || all;
    });
});
router.get('/', function (req, res, next) {
    allCategory.read = false;
    typeof allCategory.categoryList == 'object' && res.render('default', allCategory) || cat.find(function(err, all, affected){
        !err && next(err);
        allCategory.title = 'Toate categoriile';
        allCategory.categoryList = affected || all;
        res.render('default', allCategory);
    });
});
router.get('/:title', function(req, res, next) {
    var title = req.param('title').replace(/\_/g, " ");
    async.series([
        function(callback) {
            cat.findOne({ title: title }, function (err, one) {
                if(err || !one) return next(err);
                allCategory.title = title;
                allCategory.read = true;
                allCategory.editCategoryData = one;
                callback()
            })
        },
        function(callback) {
            posts.find({ categoryId: allCategory.editCategoryData._id, publish: 1 }, function (err, posts) {
                if(err) return callback(err);
                allCategory.ThisPosts = posts;
                callback()
            });
        }
    ], function(err){
        if(err) return next(err);
        res.type('html');
        return res.status(!allCategory.editCategoryData ? 404 : 200).render('default', allCategory)
    });
});

module.exports = router;