/**
 * Created by wanoo21 on 16.12.2014.
 */
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var posts = require('../models/posts').Posts;
var cat = require('../models/category').Category;

var postsD = {
    title: 'Toate postările'
};
router.all('*',function(req, res, next) {
    if(!postsD.categoryList){
        cat.find(function(err, all){
            !err && next(err);
            postsD.categoryList = all;
            next()
        })
    } else next();
});
router.get('/',function(req, res, next) {
    posts.find(function (err, posts) {
        postsD.title = 'Toate postările';
        postsD.posts = posts;
        res.render('posts', postsD) && next();
    });
});
router.get('/:title', function(req, res, next){
    var title = req.param('title').replace(/\_/g, " ");
    posts.findOne({ title: title, publish: 1 }, function(err, post){
        err && next(err);
        if(!err) post.categoryList = postsD.categoryList;
        res.render('posts', post || { title: "Nu sunt postări" });
    })
});

module.exports = router;