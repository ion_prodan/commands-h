var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var users = require('../models/users').Users;
var cat = require('../models/category').Category;
var posts = require('../models/posts').Posts;
var sanitizer = require('sanitizer');

var authentication = function(useremail, password, callback) {
    users.findOne({ email: useremail }, function(err, user, next) {
        if(err) next(err);
        if(user == null) return callback({error: true, message: '<strong>Warning</strong> This email is not found'});
        if(user.isValidPassword(password)) {
            user.passwdHash = user.salt = null;
//            console.dir(user)
            return callback({error: false}, user);
        }
        return callback({error: true, message: '<strong>Warning</strong> Password is invalid'})
    });
};
var themeParser = {
    title: 'Dashboard',
    url: '/dash/'
};
router.get('/:controller', function(req, res, next){
    "use strict";
    var controller = req.param('controller');
    if(controller == 'logout'){
        req.session.data = null;
        return res.redirect('/');
    }
    var con = controller == 'category' ? cat : posts;
    con.find(function(err, maincat){
        if(err) next(err);
        themeParser.control = controller;
        themeParser[controller] = maincat;
        themeParser.editCategory = false;
        res.render('dashboard', themeParser);
    });
});
router.get('/:controller/:id/:num', function (req, res, next) {
    "use strict";
    var controller = req.param('controller'), id = req.param('id');
    themeParser.control = controller;
    if(mongoose.Types.ObjectId.isValid(id)){
        var con = controller == 'category' ? cat : posts;
        con.findOne({ _id: id }, function(err, category){
            if(err) next(err);
            themeParser.editCategoryData = category;
            themeParser.editCategory = true;
            res.render('dashboard', themeParser);
        });
    } else {
        themeParser.editCategory = true;
        themeParser.editCategoryData = false;
        themeParser.addnew = true;
        res.render('dashboard', themeParser);
    }

}).post('/:controller/:id/:num', function (req, res, next) {
    "use strict";
    var controller = req.param('controller'), id = req.param('id'), num = req.param('num');
    var con = controller == 'category' ? cat : posts;
    if(mongoose.Types.ObjectId.isValid(id)){
        var updateInfo = {
            title: !req.body.title ? 'Title is empty' : sanitizer.escape(req.body.title),
            body: !req.body.body ? 'Body is empty' : req.body.body,
            helper: sanitizer.escape(req.body.helper),
            tags: sanitizer.escape(req.body.tags),
            publish: sanitizer.escape(req.body.publish),
            categoryId: sanitizer.escape(req.body.postsCategoryId)
        };
        con.findOneAndUpdate({ _id: req.param('id') }, updateInfo, function(err, category){
            if(err) next(err);
            themeParser.control = req.param('controller');
            themeParser.editCategoryData = category;
            //In loc de __v trebue de pus numarul adevarat a locului in masiv
//            themeParser[controller][num].title = category.title;
            themeParser.editCategory = true;
            res.render('dashboard', themeParser);
        });
    } else {
//        Aici trebue de lucrat asupra adaugarii categoriilor si postarilor
        if(req.body.postsCategoryId){
            var post = new posts();
            post.title = sanitizer.escape(req.body.title);
            post.categoryId = sanitizer.escape(req.body.postsCategoryId);
            post.publish = sanitizer.escape(req.body.publish);
            post.helper = sanitizer.escape(req.body.helper);
            post.body = req.body.body;
            post.tags = sanitizer.escape(req.body.tags);
            post.save(function(err, product, numberAffected){
                if(err) next(err);
                if(numberAffected == 1)
                    themeParser.editCategoryData = product;
                res.redirect('back');
            });
        } else {
            var category = new cat();
            category.title = sanitizer.escape(req.body.title);
            category.body = req.body.body;
            category.save(function(err, product, numberAffected){
                if(err) next(err);
                if(numberAffected == 1)
                    themeParser.editCategoryData = product;
                res.render('dashboard', themeParser);
            });
        }
    }
}).delete('/:controller/:id/:num', function (req, res, next) {
    var controller = req.param('controller'), id = req.param('id'), num = req.param('num');
    var con = controller == 'category' ? cat : posts;
    if(mongoose.Types.ObjectId.isValid(id)){
        return con.remove({ _id: id }, function(err, category){
            if(err) next(err);
            themeParser.control = req.param('controller');
            delete themeParser[controller][num];
            themeParser.editCategory = true;
            return res.redirect('/dash/'+controller);
        });
    }
});
router.all('/', function(req, res, next) {
    "use strict";
 //   var user = new users();
 //   user.email = "ionprodan33@gmail.com";
 //   user.name = "Ion Prodan";
 //   user.setPassword("1234qwer");
 //   user.save();
    if (!req.body.useremail) {
        posts.find(function(err, maincat){
            if(err) next(err);
            themeParser.editCategoryData = maincat;
            themeParser.editCategory = false;
            res.render('dashboard', themeParser);
        });
    } else {
        var useremail = req.body.useremail, userpassword = req.body.userpassword;
        authentication(useremail, userpassword, function(data, user){
            themeParser.data = req.session.data = user || data;
            res.render('dashboard', themeParser);
        })
    }
});

module.exports = router;