var express = require('express');
var router = express.Router();
var cat = require('../models/category').Category;

var indexData = {
    title: 'Blog cu idei simple'
};
/* GET home page. */
router.get('/', function(req, res, next) {
    indexData.data = req.session.data || false;
    cat.find(function (err, all) {
        !err && next(err);
        indexData.categoryList = all;
        res.render('index', indexData);
    });
});

module.exports = router;
