app = angular.module 'app', ['mgcrea.ngStrap', 'ngAnimate']
app.config ($asideProvider) ->
  angular.extend $asideProvider.defaults,
    animation: 'am-fade-and-scale'

app.controller 'editCategory', ['$scope', '$aside', (scope, aside) ->
    scope.editCategoryButton = (cat) ->
      aside
        title: 'Add new '+cat
        contentTemplate: '/partials/asideAddcategory.html'
  ]