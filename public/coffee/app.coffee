#console.dir locals if locals
$('#login_form').on "submit", (e) ->
  inputs = $(@).find 'input'
  inputs.each (k, v) ->
    if !v.value.length
      e.preventDefault()
#      $(v).attr('type') == 'email' and v.value.length or
      $(v).closest '.form-group'
        .removeClass 'has-success'
        .addClass 'has-error'
      focusable = inputs.filter -> !@.value.length
      focusable.first().focus().tooltip "show"
    else
      if $(v).attr('type') == 'email' and !/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test v.value
        e.preventDefault()
        $(@).closest '.form-group'
          .removeClass 'has-success'
          .addClass 'has-error'
        $(@).attr 'data-original-title', 'Email is invalid'
          .focus().tooltip 'show'
      else
        $(v).closest '.form-group'
          .removeClass 'has-error'
          .addClass 'has-success'
$(document).ready ->
  $('#deleteData').on 'click', (e) ->
    e.preventDefault()
    $.ajax
      url : location.pathname
      method : 'delete'



