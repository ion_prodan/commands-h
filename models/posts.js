var mongoose = require('../lib/mongoose');
var Schema = mongoose.Schema;

var postsSchema = new Schema({
    title: {
        type: String,
        unique: true
    },
    body: {
        type: String,
        unique: true,
        required: true
    },
    tags: {
      type: String
    },
    publish: {
        type: Number
    },
    categoryId: {
        type: String
    },
    helper: {
        type: Array
    },
    created: {
        type: Date,
        default: Date.now
    }
});

exports.Posts = mongoose.model('Posts', postsSchema);