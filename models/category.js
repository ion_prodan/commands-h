var mongoose = require('../lib/mongoose');
var Schema = mongoose.Schema;

var categorySchema = new Schema({
    title: {
        type: String,
        unique: true,
        required: true
    },
    body: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

exports.Category = mongoose.model('Category', categorySchema);