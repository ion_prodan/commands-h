var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('cookie-session');
var config = require('./lib/config');
// Routes
var routes = require('./routes/index');
var users = require('./routes/users');
var commands = require('./routes/cat');
var dash = require('./routes/dash');
var post = require('./routes/post');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//app.set('env', 'production');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    keys: ['31e6a7de72799d9cd2469a064d5f82bf', 'a75e32f4e83d8e6b489b73d44b65a9d1'],
    secureProxy: false // if you do SSL outside of node
}));
app.use(require('express-force-domain')('http://'+config.get('host')+':'+config.get('port')));

app.use('/', routes);
app.use('/users', users);
app.use('/category', commands);
app.use('/dash', dash);
app.use('/post', post);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: app.get('env') === 'development' ? err : {}
    });
});


module.exports = app;
